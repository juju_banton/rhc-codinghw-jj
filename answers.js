function PizzaDeliveryRun(...names){

  let num_delivery_persons = names.length;

  this.deliverers = names;

  this._parseMoves = function (moves = ""){
    let moves_arr = [];
    const moveList = ["<",">","^","v"];

    for (let i = 0; i < moves.length; i++) {
      let m = moves[i];
      if (moveList.includes(m)){
        moves_arr.push(m);
      }
    }
    return moves_arr;
  };

  this._addVisitedLocation =  function (move,coord){
    let lastMove = coord;
    let x;
    let y;
    let nextMove;

    switch (move) {
      case "<":
        x = lastMove[0] - 1;
        y = lastMove[1];
        break;
      case ">":
        x = lastMove[0] + 1;
        y = lastMove[1];
        break;
      case "^":
        x = lastMove[0];
        y = lastMove[1] + 1;
        break;
      case "v":
        x = lastMove[0];
        y = lastMove[1] - 1;
        break;
      default:
        console.log("Invalid Move Detected");
        break;
    }
    nextMove = [x, y];
    return nextMove;
  };

  this.allHousesVisited = function(moves, n = num_delivery_persons ) {
    let allmoves = [[0,0]];
    let housesVisted = {};
    let moves_arr = this._parseMoves(moves);
    let i2;
    let prev;
    let next;

    moves_arr.forEach((m,i1)=> {
      i2 = i1 - (n - 1);
      if (i2 < 0) { i2 = 0; }
      prev = allmoves[i2];
      next = this._addVisitedLocation(m, prev);
      allmoves.push(next);
    });

    allmoves.forEach(coord =>{
      housesVisted[coord] = true;
    });
    return Object.keys(housesVisted).length;
  };

}

module.exports = PizzaDeliveryRun;
