const assert = require("chai").assert;
const PizzaDeliveryRun = require("../answers.js");

describe("PizzaDeliveryRuns",()=>{
  describe("Pizza Delivery Run With Maria",()=>{
    let mariasDeliveryRun = new PizzaDeliveryRun("Maria");
    it("allHousesVisited('<>') should return a number",()=>{
      let moves = "<><>";
      let result = mariasDeliveryRun.allHousesVisited(moves);
      assert.typeOf(result,'number');
    });

    it("should return 1 for no input",()=>{
      let result = mariasDeliveryRun.allHousesVisited();
      assert.equal(result,1);
    });

    it("should account for none standard input: 'left' ",()=>{
      let result = mariasDeliveryRun.allHousesVisited('left');
      assert.equal(result,1);
    });

    it("should return 2 for '>'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('>');
      assert.equal(result,2);
    });

    it("should return 4 for '^>v<'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('^>v<');
      assert.equal(result,4);
    });

    it("should return 2 for '^v^v^v^v^v'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('^v^v^v^v^v');
      assert.equal(result,2);
    });

    it("should return 2 for '<><><>'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('<><><>');
      assert.equal(result,2);
    });

    it("should return 6 for '^^^^ ^'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('^^^^ ^');
      assert.equal(result,6);
    });

    it("should return 8 for '^ > ^ v > > > < ^'",()=>{
      let result = mariasDeliveryRun.allHousesVisited('^ > ^ v > > > < ^');
      assert.equal(result,8);
    });

  });


  describe("mariasAndColvisDeliveries()",()=>{
    let mariasAndColvisDeliveries = new PizzaDeliveryRun("Maria","Clovis");
    it("should return a number",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('<^');
      assert.equal(result,3);
    });

    it("should return 1 for no input",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited();
      assert.equal(result,1);
    });

    it("should return 2 for '>'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('>');
      assert.equal(result,2);
    });

    it("should return 3 for '^v'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('^v');
      assert.equal(result,3);
    });

    it("should return 3 for '^>v<'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('^>v<');
      assert.equal(result,3);
    });

    it("should return 7 for '<><> <>'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('<><> <>');
      assert.equal(result,7);
    });

    it("should return 11 for '^v^v^v^v^v'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('^v^v^v^v^v');
      assert.equal(result,11);
    });

    it("should return 9 for '^>^v>>><^'",()=>{
      let result = mariasAndColvisDeliveries.allHousesVisited('^>^v>>><^');
      assert.equal(result,9);
    });

  });
});
